# Casey Example Project

It is a implementation of Peterson algorithm, for showcase of [Casey Project](https://gitlab.com/k1ab/casey).

More information about Peterson algorithm is [here](https://www.geeksforgeeks.org/petersons-algorithm-for-mutual-exclusion-set-1/)

## Running the algorithm

    $ gcc peterson_main.c -o peterson_main
    $ ./peterson_main
